# Load data from file
disp("Loading data");
load /home/stefan/features/beec.mp3.feature;

disp("Splitting data");
# break data into corresponding features
zero_crossing_rate          = features(:,1) / norm(features(:,1));
first_order_autocorrelation = features(:,2) / norm(features(:,2));
energy                      = features(:,3) / norm(features(:,3));
linear_regression           = features(:,4) / norm(features(:,4));
spectral_smoothness         = features(:,5) / norm(features(:,5));
spectral_centroid           = features(:,6) / norm(features(:,6));
spectral_spread             = features(:,7) / norm(features(:,7));
spectral_dissymmetry        = features(:,8) / norm(features(:,8));


disp("Ploting data");
x = 1 : length(zero_crossing_rate);
subplot(4,2,1);
plot(x, zero_crossing_rate);
title("zero\_crossing\_rate");

subplot(4,2,3);
plot(x, first_order_autocorrelation);
title("first\_order\_autocorrelation");

subplot(4,2,5);
plot(x, energy);
title("energy");

subplot(4,2,7);
plot(x, linear_regression);
title("linear\_regression");

subplot(4,2,2);
plot(x, spectral_smoothness);
title("spectral\_smoothness");

subplot(4,2,4);
plot(x, spectral_centroid);
title("spectral\_centroid");

subplot(4,2,6);
plot(x, spectral_spread);
title("spectral\_spread");

subplot(4,2,8);
plot(x, spectral_dissymmetry);
title("spectral\_dissymmetry");
