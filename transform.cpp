#include <iostream>
#include <fstream>
#include <string>

int main(int argc, char** argv){
	std::string input(argv[1]);
	std::ifstream inFile;
	std::ofstream outFile;
	inFile.open(input, std::ios::in);
	outFile.open(std::string("transformed_") + input, std::ios::out);
	std::string line;
	int i = 0;
	while(std::getline(inFile,line)){
		outFile << i << " " << line << std::endl;
		++i;
	}
	inFile.close();
	outFile.close();
}
