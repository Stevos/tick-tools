#include <iostream>
#include <fstream>
#include <string>

int main(int argc, char** argv){
	std::string input1(argv[1]);
	std::string input2(argv[2]);
	
	std::ifstream in1File;
	std::ifstream in2File;
	
	in1File.open(input1, std::ios::in);
	in2File.open(input2, std::ios::in);
	int16_t t1,t2;
	int i = 0;
	
	while((in1File >> t1) && (in2File >> t2)){
		
		if(t1 != t2){
			std::cout << "FAIL!" << std::endl;
			return -1;
		}
		
	}
	
	std::cout << "SUCCESS!" << std::endl;
	
	in1File.close();
	in2File.close();
	
	return 0;
}
