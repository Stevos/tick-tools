#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>

#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QString ret = QFileDialog::getOpenFileName(this, tr("Open File"),
                                               "/home/stefan/test_out/features/",
                                               tr("Features (*.ftrs)"));

    m_charts = new FeatureCharts(ret.toUtf8().constData());
    m_charts->generateCharts();
    file1 = ret;
    ui->label->setText(file1);
}

void MainWindow::on_comboBox_activated(const QString &arg1)
{
    uint choice = 0;
    if(arg1 == "ZERO_CROSSING_RATE")            choice = 0;
    if(arg1 == "FIRST_ORDER_AUTOCORRELATION")   choice = 1;
    if(arg1 == "RMS_ENERGY")                    choice = 2;
    if(arg1 == "MAX_AMPLITUDE")                 choice = 3;
    if(arg1 == "SPECTRAL_CENTROID")             choice = 4;
    if(arg1 == "LINEAR_REGRESSION")             choice = 5;
    if(arg1 == "SPECTRAL_SMOOTHNESS")           choice = 6;
    if(arg1 == "SPECTRAL_SPREAD")               choice = 7;
    if(arg1 == "SPECTRAL_DISSYMETRY")           choice = 8;

    m_charts->getChart(choice)->setTitle(file1 + "      " +arg1);
    ui->centralWidget = m_charts->getChartView(choice);
    ui->centralWidget->show();

}

void MainWindow::on_comboBox_2_activated(const QString &arg1)
{
    uint choice = 0;
    if(arg1 == "ZERO_CROSSING_RATE")            choice = 0;
    if(arg1 == "FIRST_ORDER_AUTOCORRELATION")   choice = 1;
    if(arg1 == "RMS_ENERGY")                    choice = 2;
    if(arg1 == "MAX_AMPLITUDE")                 choice = 3;
    if(arg1 == "SPECTRAL_CENTROID")             choice = 4;
    if(arg1 == "LINEAR_REGRESSION")             choice = 5;
    if(arg1 == "SPECTRAL_SMOOTHNESS")           choice = 6;
    if(arg1 == "SPECTRAL_SPREAD")               choice = 7;
    if(arg1 == "SPECTRAL_DISSYMETRY")           choice = 8;

    m_charts2->getChart(choice)->setTitle(file2+" \n" +  file1 +" " +arg1);

    QPen pen(QRgb(0xff0000));
    m_charts->getSeries(choice)->setPen(pen);


    m_charts2->getChart(choice)->addSeries(m_charts->getSeries(choice));
    m_charts2->getChartView(choice)->show();

}

void MainWindow::on_pushButton_2_clicked()
{
    QString ret = QFileDialog::getOpenFileName(this, tr("Open File"),
                                               "/home/stefan/test_out/features/",
                                               tr("Features (*.ftrs)"));

    m_charts2 = new FeatureCharts(ret.toUtf8().constData());
    m_charts2->generateCharts();
    file2 = ret;
    ui->label_2->setText(file2);
}
