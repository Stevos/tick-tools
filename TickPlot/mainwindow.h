#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <featurecharts.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_comboBox_activated(const QString &arg1);

    void on_comboBox_2_activated(const QString &arg1);

    void on_pushButton_2_clicked();

private:
    QString file1;
    QString file2;

    Ui::MainWindow *ui;
    FeatureCharts* m_charts;
    FeatureCharts* m_charts2;

};

#endif // MAINWINDOW_H
