#ifndef BLOCKCHART_H
#define BLOCKCHART_H

#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QLogValueAxis>
#include <QtCharts/QValueAxis>


#include <string>
#include <vector>

QT_CHARTS_USE_NAMESPACE

using VVFloat = std::vector<std::vector<float>>;

class BlockCharts
{
public:
    BlockCharts(const std::string& path);
    void generateCharts();
    QChart* getChart(size_t index);
    QChartView* getChartView(size_t index);
    QLineSeries* getSeries(size_t index) { return m_series[index]; }

private:
    const size_t featureNum = 9;

    std::string               m_path;
    std::vector<QLineSeries*> m_series;
    std::vector<QChart*>      m_charts;
    std::vector<QChartView*>  m_chart_views;

    void readData(const std::string& path);
};

#endif // BLOCKCHART_H
