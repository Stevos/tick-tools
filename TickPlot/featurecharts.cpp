#include "featurecharts.h"


#include <iostream>
#include <fstream>

FeatureCharts::FeatureCharts(const std::string& path)
    : m_path(path)
{
    for(size_t i = 0; i < featureNum; ++i)
    {
        m_series.push_back(new QLineSeries());
    }
}

void FeatureCharts::readData(const std::string &path)
{
    std::ifstream file(path);

    uint32_t index = 0;
    while(!file.eof())
    {
        for(size_t i = 0; i < featureNum; ++i)
        {
            float temp = 0.f;
            file >> temp;
            *(m_series[i]) << QPointF(index, temp);
        }
        //if(index == 1000) break;
        ++index;
    }

    file.close();
}

void FeatureCharts::generateCharts()
{
    readData(m_path);
    for(size_t i = 0; i < featureNum; ++i)
    {
        QChart* chart = new QChart();
        chart->addSeries(m_series[i]);
        chart->legend()->hide();
        chart->createDefaultAxes();

        m_charts.push_back(chart);
        m_chart_views.push_back(new QChartView(chart));
        m_chart_views.back()->setRenderHint(QPainter::Antialiasing);
    }
}


QChart* FeatureCharts::getChart(size_t index) {
    return m_charts[index];
}

QChartView* FeatureCharts::getChartView(size_t index)
{
    return m_chart_views[index];
}

