#!/bin/bash

# This is script intended to provide simple enviroment for bash
# to ease deployment. Basic idea is to source PATH and make
# custom utilities available in used bash konsole.

#
#
#
#

# For ploting output log data
sudo apt-get install xgraph

# Install dependecies
sudo apt-get install build-essential

#  Compile and add transform log tool
g++ -std=c++11 transform.cpp -o addXcomp

# Add path

export PATH="`pwd .`/:$PATH"
